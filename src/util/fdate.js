const ddigit = v=> v<10? '0'+v: v
const dateStamp = d=> !d? '--':
	`${d.getFullYear()}-${ddigit(d.getMonth()+1)}-${ddigit(d.getDate())} ${ddigit(d.getHours())}:${ddigit(d.getMinutes())}`
export default dateStamp
